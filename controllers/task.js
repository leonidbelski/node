const Task = require('../models').Task;
const User = require('../models').User;

module.exports = {
  create(req, res) {
    return Task
      .create({
        title: req.body.title,
        iscompleted: req.body.iscompleted
      })
      .then((task) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  },

  list(req, res) {
    return Task
      .findAll({
        include: {
          model: User
        }
      })
      .then((task) => res.status(200).send(task))
      .catch((error) => res.status(500).send(error));
  },

  getTask(req, res) {
    return Task
      .findOne({
        where: {
          id: req.params.id
        },
        include: {
          model: User
        }
      })
      .then((task) => res.status(200).send(task))
      .catch((error) => res.status(500).send(error));
  },

  updateTask(req, res) {
    return Task
      .update({
          title: req.body.title,
          iscompleted: req.body.iscompleted
        },
        {
          where: { id: req.params.id }
        })
      .then((task) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  },

  deleteTask(req, res) {
    return Task
      .destroy({where: { id: req.params.id }})
      .then((task) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  }
}; 