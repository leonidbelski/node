module.exports = {
	signup(req, res) {
		return res.render('signup');
	},
	signin(req, res) {
		return res.render('signin');
	},
	dashboard(req, res) {
		return res.render('dashboard');
	},
	logout(req, res) {
		req.session.destroy(function(err) {
			res.redirect('/signin');
		});
	},
	isLoggedIn(req, res, next) {
		if (req.isAuthenticated())
			return next();
		res.redirect('/signin');
	}
};