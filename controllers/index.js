const task = require('./task');
const user = require('./user');
const project = require('./project');
const auth = require('./auth');

module.exports = {
  task,
  user,
  project,
  auth
};