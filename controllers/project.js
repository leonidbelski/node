const Project = require('../models').Project;
const User = require('../models').User;

module.exports = {
  create(req, res) {
    return Project
      .create({
        name: req.body.name
      })
      .then((project) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  },

  list(req, res) {
    return Project
      .findAll({
        include: {
          model: User
        }
      })
      .then((project) => res.status(200).send(project))
      .catch((error) => res.status(500).send(error));
  },

  getProject(req, res) {
    return Project
      .findByPk(req.params.id)
      .then((project) => res.status(200).send(project))
      .catch((error) => res.status(500).send(error));
  },

  updateProject(req, res) {
    return Project
      .update({
          name: req.body.name
        },
        {
          where: { id: req.params.id }
        })
      .then((project) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  },

  deleteProject(req, res) {
    return Project
      .destroy({where: { id: req.params.id }})
      .then((project) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  }
}; 