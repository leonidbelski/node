const User = require('../models').User;
const Project = require('../models').Project;

module.exports = {
  create(req, res) {
    return User
      .create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password
      })
      .then((user) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  },

  list(req, res) {
    return User
      .findAll({
        include: {
          model: Project
        }
      })
      .then((user) => res.status(200).send(user))
      .catch((error) => res.status(500).send(error));
  },

  getUser(req, res) {
    return User
      .findByPk(req.params.id)
      .then((user) => res.status(200).send(user))
      .catch((error) => res.status(500).send(error));
  },

  updateUser(req, res) {
    return User
      .update({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password
      },
      {
        where: { id: req.params.id }
      })
      .then((user) => res.status(200).send(user))
      .catch((error) => res.status(500).send(error));
  },

  deleteUser(req, res) {
    return User
      .destroy({where: { id: req.params.id }})
      .then((user) => res.sendStatus(200))
      .catch((error) => res.status(500).send(error));
  }
}; 