'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
  }, {});
  User.associate = function(models) {
    User.hasMany(models.Task, {foreignKey: 'userId'});
    User.belongsToMany(models.Project, {through: 'UserProject', foreignKey: 'userId'});
  };
  return User;
};