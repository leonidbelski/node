'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      firstName: 'User1',
      lastName: 'UserLast1',
      email: 'user1@demo.com',
      password: 'user1',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      firstName: 'User2',
      lastName: 'UserLast2',
      email: 'user2@demo.com',
      password: 'user2',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      firstName: 'User3',
      lastName: 'UserLast3',
      email: 'user3@demo.com',
      password: 'user3',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
