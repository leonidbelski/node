'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Projects', [{
      name: 'Project1',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Project2',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Project3',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Projects', null, {});
  }
};
