const userController = require('../controllers').user;
const User = require('../models').User;

const supertest = require("supertest");
const chai = require('chai');

const should = chai.should();

const server = supertest.agent("http://localhost:3000");

describe("Unit test",function(){
	it("should return home page",function(done){
		server
			.get("/")
			.expect(200)
			.end(function(err,res){
				res.status.should.equal(200);
				done();
			});
	});

	it("should return 404",function(done){
		server
			.get("/random")
			.expect(404)
			.end(function(err,res){
				res.status.should.equal(404);
				done();
			});
	});

	describe('/GET users', () => {
		it("should return users page",function(done){
			server
				.get("/users")
				.expect(200)
				.end(function(err,res){
					res.status.should.equal(200);
					res.body.should.be.a('array');
					done();
				});
		});
	});

	describe('/POST users', () => {
		it("should added user",function(done){
			let user = {
				firstName: "c",
				lastName: "c",
				email: "c@c.c",
				password: "c"
			};

			server
				.post("/users")
				.send(user)
				.end(function(err,res){
					res.status.should.equal(200);
					res.body.should.be.a('object');
					done();
				});
		});
	});

	describe('/GET/:id user', () => {
		it("it should GET user",function(done){
			server
				.get("/users/1")
				.expect(200)
				.end(function(err,res){
					res.status.should.equal(200);
					res.body.should.be.a('object');
					res.body.should.have.property('firstName');
					res.body.should.have.property('lastName');
					res.body.should.have.property('email');
					res.body.should.have.property('password');
					done();
				});
		});
	});

	describe('/PUT/:id user', () => {
		it("it should UPDATE user",function(done){
			let user = {
				firstName: "p",
				lastName: "p",
				email: "p@p.p",
				password: "p"
			};

			User
				.create(user)
				.then((user) => {
					server
						.put('/users/' + user.id)
						.send({firstName: "b"})
						.end((err, res) => {
							res.status.should.equal(200);
							res.body.should.be.a('object');
							done();
						});
				})
		});
	});

	describe('/DELETE/:id user', () => {
		it('it should DELETE user', (done) => {
			let user = {
				firstName: "d",
				lastName: "d",
				email: "d@d.d",
				password: "d"
			};

			User
				.create(user)
				.then((user) => {
					server
						.delete('/users/' + user.id)
						.end((err, res) => {
							res.status.should.equal(200);
							res.body.should.be.a('object');
							done();
						});
				})
		});
	});
});

