const express = require('express');
const bodyParser = require('body-parser');
const exphbs = require('express-handlebars');
const passport   = require('passport');
const session    = require('express-session');

const userController = require('./controllers').user;
const projectController = require('./controllers').project;
const taskController = require('./controllers').task;
const authController = require('./controllers').auth;
const models = require("./models");

const app = express(); 
const port = process.env.PORT || 3000;
const router = express.Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


// For Passport

app.use(session({ secret: 'keyboard cat', resave: true, saveUninitialized:true}));

app.use(passport.initialize());

app.use(passport.session());

//load passport strategies

require('./config/passport/passport.js')(passport, models.User);

app.listen(port, function () {
  console.log('API started http://localhost:' + port);
});

router.get('/', function (req, res) {
  res.status(200).send("Hello API!!!");
});

/*User*/

router.route('/users')
  .get(userController.list)
  .post(userController.create);

router.route('/users/:id')
  .get(userController.getUser)
  .put(userController.updateUser)
  .delete(userController.deleteUser);


/*Task*/

router.route('/tasks')
  .get(taskController.list)
  .post(taskController.create);

router.route('/tasks/:id')
  .get(taskController.getTask)
  .put(taskController.updateTask)
  .delete(taskController.deleteTask);


/*Project*/

router.route('/projects')
  .get(projectController.list)
  .post(projectController.create);

router.route('/projects/:id')
  .get(projectController.getProject)
  .put(projectController.updateProject)
  .delete(projectController.deleteProject);


/*Auth*/

router.route('/signup')
  .get(authController.signup)
  .post(passport.authenticate('local-signup', {
    successRedirect: '/dashboard',
    failureRedirect: '/signup'
  }));

router.route('/signin')
  .get(authController.signin)
  .post(passport.authenticate('local-signin', {
    successRedirect: '/dashboard',
    failureRedirect: '/signin'
  }));


router.route('/dashboard')
  .get(authController.isLoggedIn, authController.dashboard);

router.route('/logout')
  .get(authController.logout);


app.use('/', router);

//For Handlebars
app.set('views', './views');
app.engine('hbs', exphbs({
  defaultLayout: 'main',
  extname: '.hbs',
  layoutsDir: './views/layouts'
}));
app.set('view engine', '.hbs');